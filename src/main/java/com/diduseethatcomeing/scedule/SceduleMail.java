package com.diduseethatcomeing.scedule;

import com.diduseethatcomeing.config.Configuration;
import com.diduseethatcomeing.model.User;
import com.diduseethatcomeing.security.SecurityAuthenticationFailureHandler;
import com.diduseethatcomeing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
public class SceduleMail {

    @Autowired
    private UserService userService;

    private final Configuration configuration;
    public SceduleMail(Configuration configuration) {
        this.configuration = configuration;
    }

     //@Scheduled(fixedRate = 15*1000)
    public void sendEmail(HttpSession session, String body){
       User user = (User) session.getAttribute("userPending");
        userService.sendEmail(
                configuration.getFrom(),
                user.getEmail(),
                "aktivlestirme kodu",
                body
        );
    }


}
