package com.diduseethatcomeing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class SpriingSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpriingSecurityApplication.class, args);
	}

}
