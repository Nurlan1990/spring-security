package com.diduseethatcomeing.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Arrays;

public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private Configuration configuration;


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        System.out.println("allowed origins = " + Arrays.toString(configuration.getOrigins()));


        registry.addMapping("/**")
//                .allowedOrigins("http://localhost:8080", "http://localhost:63342")
//                .allowedOrigins("*")
                .allowedOrigins(configuration.getOrigins())
                .allowedMethods(HttpMethod.GET.name(),
                        HttpMethod.POST.name(),
                        HttpMethod.PUT.name(),
                        HttpMethod.DELETE.name()
                );
    }


}
