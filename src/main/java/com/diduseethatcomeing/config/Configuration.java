package com.diduseethatcomeing.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Properties;

@Component
@ConfigurationProperties(prefix = "didusee")
public class Configuration {

    private String from;
    private String[] origins;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String[] getOrigins() {
        return origins;
    }

    public void setOrigins(String[] origins) {
        this.origins = origins;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "from='" + from + '\'' +
                ", origins=" + Arrays.toString(origins) +
                '}';
    }
}
