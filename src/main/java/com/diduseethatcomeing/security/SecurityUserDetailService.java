package com.diduseethatcomeing.security;

import com.diduseethatcomeing.model.User;
import com.diduseethatcomeing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.Optional;

@Service
public class SecurityUserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userService.getUserByEmail(username);
        if(optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setRoleList(userService.getUserRoles(user.getId()));
            UserPrincipal principal = new UserPrincipal(user);
            //System.out.println("user = " + user);
            return principal;
        } else {
            throw new UsernameNotFoundException("User " + username + " not found!");
        }
    }
}
