package com.diduseethatcomeing.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class Config extends WebSecurityConfigurerAdapter {

    @Autowired
    private SecurityUserDetailService userDetailsService;

    @Autowired
    private SecurityAuthenticationSuccessHandler authenticationSuccessHandler;

    @Autowired
    private SecurityAuthenticationFailureHandler authenticationFailureHandler;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
      /*  http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .httpBasic()
                .and().authorizeRequests()
                .antMatchers(HttpMethod.GET, "/login").permitAll()
                .antMatchers(HttpMethod.GET, "/tesdiqkodu").hasAnyRole("COMPANY", "CANDIDATE", "ADMIN")
                .antMatchers(HttpMethod.POST, "/*").hasAnyRole("COMPANY", "CANDIDATE", "ADMIN")
                .antMatchers(HttpMethod.PUT, "/*").hasAnyRole("COMPANY", "CANDIDATE", "ADMIN")
                .antMatchers(HttpMethod.DELETE, "/*").hasAnyRole("COMPANY", "CANDIDATE", "ADMIN")
                .and().formLogin().disable()
                .csrf().disable();*/


        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/candidate/**").hasAnyRole("CANDIDATE","ADMIN")
                .antMatchers("/").permitAll()
                .and().csrf().disable()
                .formLogin()
                .loginPage("/login")
                .usernameParameter("email")
                .successHandler(authenticationSuccessHandler)
                .failureHandler(authenticationFailureHandler);



    }

    @Bean
    public PasswordEncoder getPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(getPasswordEncoder());
        return authenticationProvider;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        /*auth.inMemoryAuthentication()
                .withUser("ali@gmail.com")
                .password("123456")
                .roles("ADMIN")
                .and()
                .withUser("tofiq@canada.com")
                .password("canada")
                .roles("CANDIDATE");*/
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }
}
