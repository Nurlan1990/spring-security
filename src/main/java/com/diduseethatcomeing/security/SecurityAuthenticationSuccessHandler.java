package com.diduseethatcomeing.security;

import com.diduseethatcomeing.model.User;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class SecurityAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        UserPrincipal principal = (UserPrincipal) authentication.getPrincipal();
        User user = principal.getUser();

        HttpSession session = request.getSession();
        session.setAttribute("user", user);
        session.setAttribute("loginTime", LocalDateTime.now());

        //System.out.println("logged in user = " + user);
        String page = user.getRoleList().get(0).getPage();
        response.sendRedirect(request.getContextPath() + page);

    }
}
