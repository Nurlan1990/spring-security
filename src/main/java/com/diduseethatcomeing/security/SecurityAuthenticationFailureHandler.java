package com.diduseethatcomeing.security;

import com.diduseethatcomeing.model.User;
import com.diduseethatcomeing.scedule.SceduleMail;
import com.diduseethatcomeing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;
import java.util.Random;


@Component
public class SecurityAuthenticationFailureHandler  implements AuthenticationFailureHandler {

    @Autowired
    private UserService userService;

    @Autowired
    private SceduleMail sceduleMail;


    static int entrance_fee = 3;


    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
                                        HttpServletResponse response,
                                        AuthenticationException exception) throws IOException, ServletException {

        //1. Exception novune gore ferqli sehifelere yonlendirmek

        String message = "";

        if(exception.getClass() == BadCredentialsException.class) {
            //2. 3 defe ardicil parol sehv yazilsa emaile-e tesdiq kodu göndərir

            String userEmail = request.getParameter("email");
            Optional<User> optionalUser = userService.getUserByEmail(userEmail);

            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                user.setRoleList(userService.getUserRoles(user.getId()));

                entrance_fee--;
                System.out.println("entrance_fee =" + entrance_fee);

                if (entrance_fee == 0) {
                    userService.updateUserStatus(user.getId());

                    optionalUser = userService.getUserByEmail(userEmail);
                    user = optionalUser.get();
                    user.setRoleList(userService.getUserRoles(user.getId()));

                    HttpSession session = request.getSession();
                    session.setAttribute("userPending", user);
                    System.out.println("user = " + user);

                    //  generate code;
                    Random rand = new Random();
                    int code = rand.nextInt(100000) + 1000;

                    //  updateCode
                    userService.updateCode(code, user.getId());


                    //   send code to user's email
                    String body = String.valueOf(code);
                    sceduleMail.sendEmail(session,body);

                    response.sendRedirect(request.getContextPath() + "/tesdiqkodu");

                } else {
                    response.sendRedirect(request.getContextPath() +"/login");
                }
            } else {
                response.sendRedirect(request.getContextPath() +"/login");
            }
        } else if (exception.getClass() == DisabledException.class){
            message = "user is disabled";
            message = message.replaceAll(" ","");
            response.sendRedirect(request.getContextPath() + String.format("/%s", message));
        } else if (exception.getClass() == LockedException.class){
            message = "user account is locked";
            message = message.replaceAll(" ","");
            response.sendRedirect(request.getContextPath() + String.format("/%s", message));
        }


        System.out.println("exception = " + exception);
        System.out.println("xeta = " + exception.getMessage());


    }


    public static int getEntrance_fee() {
        return entrance_fee;
    }

    public static void setEntrance_fee(int entrance_fee) {
        SecurityAuthenticationFailureHandler.entrance_fee = entrance_fee;
    }


}
