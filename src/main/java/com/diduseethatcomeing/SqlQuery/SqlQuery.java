package com.diduseethatcomeing.SqlQuery;

public class SqlQuery {

    public static final String GET_USER_BY_EMAIL = "select id, name, surname, phone , " +
            " mobile , user_status_id, " +
            " email, password, idate, udate " +
            " from user " +
            "where email = :email and status = 1";


    public static final String GET_USER_ROLES = "select r.id, r.name,r.page, r.priority " +
            " from user_role ur join user u on ur.user_id = u.id " +
            " join role r on ur.role_id = r.id " +
            " where u.id = :user_id "+
            " order by r.priority";

    public static String GET_USER_CODE = "select code " +
            "from user " +
            "where id=:userId and " +
            "status = 1";

    public static final String UPDATE_USER_STATUS_PENDING_BY_ID = "update user set user_status_id=0  " +
            " where id = :userId ";

    public static final String UPDATE_USER_STATUS_ACTIVE_BY_ID = "update user set user_status_id=1  " +
            " where id = :userId ";

    public static final String UPDATE_USER_CODE_BY_ID = "update user set code=:code  " +
            " where id = :userId ";
}
