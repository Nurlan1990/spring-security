package com.diduseethatcomeing.controller;

import com.diduseethatcomeing.model.User;
import com.diduseethatcomeing.security.SecurityAuthenticationFailureHandler;
import com.diduseethatcomeing.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@RequestMapping("/")
@RestController
public class WebController {


    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;



    @GetMapping("/")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("web/index");
        return modelAndView;
    }

    @GetMapping("/login")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView("web/login");
        return modelAndView;
    }


    @GetMapping("/userisdisabled")
    public ModelAndView statusPending() {
        ModelAndView modelAndView = new ModelAndView("web/userisdisabled");
        return modelAndView;
    }

    @GetMapping("/useraccountislocked")
    public ModelAndView statusLocked() {
        ModelAndView modelAndView = new ModelAndView("web/statuslocked");
        return modelAndView;
    }

    @GetMapping("/cannotfindauserorpassword")
    public ModelAndView usernameNotFound() {
        ModelAndView modelAndView = new ModelAndView("web/wronguserorpass");
        return modelAndView;
    }

    @GetMapping("/tesdiqkodu")
    public ModelAndView testCodu() {
        ModelAndView modelAndView = new ModelAndView("web/tesdiqkodu");
        return modelAndView;
    }

    @PostMapping("/tesdiq")
    public ModelAndView activateProfile(@RequestParam(name = "code") int code,
                                        HttpSession session) {
        ModelAndView modelAndView =new ModelAndView();

        User user = (User) session.getAttribute("userPending");

        int databaseCode = userService.getCodeByUserId(user.getId());
        System.out.println("code =" + code);

        if(code==databaseCode){
            userService.updateUserStatusActive(user.getId());
            SecurityAuthenticationFailureHandler.setEntrance_fee(3);
            modelAndView.setViewName("web/login");
        }


        return modelAndView;
    }











}
