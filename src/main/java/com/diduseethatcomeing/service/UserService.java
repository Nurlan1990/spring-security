package com.diduseethatcomeing.service;

import com.diduseethatcomeing.model.Role;
import com.diduseethatcomeing.model.User;
import com.diduseethatcomeing.repo.UserRepository;
import com.diduseethatcomeing.security.SecurityAuthenticationFailureHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JavaMailSender mailSender;


    public Optional<User> getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    public List<Role> getUserRoles(long userId) {
        return userRepository.getUserRoles(userId);
    }

    public int getCodeByUserId(long userId){
        return userRepository.getCodeByUserId(userId);
    }


    public void updateUserStatus(long userId){
         userRepository.updateUserStatus(userId);
    }

    public void updateUserStatusActive(long userId) { userRepository.updateUserStatusActive(userId); }


    public void updateCode(int code, long userId) {
        userRepository.updateCode(code,userId);
    }


    @Async
    public void sendEmail(String from, String to, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);

        mailSender.send(message);

    }
}
