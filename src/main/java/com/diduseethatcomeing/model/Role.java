package com.diduseethatcomeing.model;

import java.util.Arrays;

public enum Role {
    CANDIDATE(1), COMPANY(2), ADMIN(3);

    private int value;
    private String page;
    private int priority;


    Role(int value, String page) {
        this.value = value;
        this.page = page;
    }

    Role(int value) {
        this.value = value;
    }


    public static Role fromValue(int value) {
       Role type = null;

//        if(value == 1) {
//            type = CANDIDATE;
//        } else if(value == 2) {
//            type = COMPANY_REP;
//        } else if(value == 3) {
//            type = ADMIN;
//        } else {
//            throw new RuntimeException("Invalid user type " + value);
//        }

        type = Arrays.stream(values())
                .filter(role -> role.getValue() == value)
                .findFirst().orElseThrow(() -> new RuntimeException("Invalid role " + value));

        return type;
    }


    public int getValue() {
        return value;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public String getPage() {
        return page;
    }
}
