package com.diduseethatcomeing.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Candidate implements Serializable {
    private static final long serialVersionUID = -7422001752754129186L;

    protected long id;
    protected LocalDateTime insertDate;
    protected LocalDateTime lastUpdate;
    private User user;
    private LocalDate birthDate;

    public Candidate() {
        this.user = new User();
        this.user.getRoleList().add(Role.CANDIDATE);
        this.birthDate = null;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDateTime getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDateTime insertDate) {
        this.insertDate = insertDate;
    }

    public LocalDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(LocalDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public String toString() {
        return "Candidate{" +
                "id=" + id +
                ", insertDate=" + insertDate +
                ", lastUpdate=" + lastUpdate +
                ", user=" + user +
                ", birthDate=" + birthDate +
                '}';
    }
}
