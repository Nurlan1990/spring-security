package com.diduseethatcomeing.repo;

import com.diduseethatcomeing.SqlQuery.SqlQuery;
import com.diduseethatcomeing.model.Role;
import com.diduseethatcomeing.model.User;
import com.diduseethatcomeing.repo.mapper.RoleRowMapper;
import com.diduseethatcomeing.repo.mapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private UserRowMapper userRowMapper;

    @Autowired
    private RoleRowMapper roleRowMapper;

    public Optional<User> getUserByEmail(String email) {
        Optional<User> optionalUser = Optional.empty();

        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("email", email);

        List<User> users = jdbcTemplate.query(SqlQuery.GET_USER_BY_EMAIL, params, userRowMapper);

        if (!users.isEmpty()) {
            optionalUser = Optional.of(users.get(0));
        }

        return optionalUser;
    }

    public List<Role> getUserRoles(long userId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("user_id", userId);
        System.out.println("aaaaa="+jdbcTemplate.query(SqlQuery.GET_USER_ROLES, params, roleRowMapper));
        return jdbcTemplate.query(SqlQuery.GET_USER_ROLES, params, roleRowMapper);
    }

    public int getCodeByUserId(long userId) {
       return jdbcTemplate.queryForObject(
                SqlQuery.GET_USER_CODE,
                new MapSqlParameterSource().addValue("userId", userId),
                Integer.class);

    }

    public void updateUserStatus(long userId) {

        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("userId", userId);
        int count = jdbcTemplate.update(SqlQuery.UPDATE_USER_STATUS_PENDING_BY_ID,mapSqlParameterSource);
        if (count > 0) {
            System.out.println(userId + " ugurla update olundu ");
        }
        else {
            throw new RuntimeException(userId + " yenilenmesinde prablem bas verdi !");
        }

    }

    public void updateUserStatusActive(long userId) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("userId", userId);
        int count = jdbcTemplate.update(SqlQuery.UPDATE_USER_STATUS_ACTIVE_BY_ID, mapSqlParameterSource);
        if (count > 0) {
            System.out.println(userId + " ugurla update olundu ");
        }
        else {
            throw new RuntimeException(userId + " yenilenmesinde prablem bas verdi !");
        }
    }

    public void updateCode(int code, long userId) {
        MapSqlParameterSource mapSqlParameterSource = new MapSqlParameterSource()
                .addValue("userId", userId)
                .addValue("code",code);
        int count = jdbcTemplate.update(SqlQuery.UPDATE_USER_CODE_BY_ID, mapSqlParameterSource);
        if (count > 0) {
            System.out.println(userId + " ugurla update olundu ");
        }
        else {
            throw new RuntimeException(userId + " yenilenmesinde prablem bas verdi !");
        }
    }
}
