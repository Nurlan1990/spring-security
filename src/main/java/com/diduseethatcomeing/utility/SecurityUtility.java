package com.diduseethatcomeing.utility;


import com.diduseethatcomeing.model.Role;
import com.diduseethatcomeing.model.User;

public class SecurityUtility {

    public static boolean hasRole(User user, Role role) {
        return user.getRoleList()
                .stream()
                .anyMatch(userRole -> userRole == role);
    }
}
